'use strict';

const express = require('express');

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();
app.get('/', function (req, res) {
  res.send('Hello world\\nHello Imran\\n');
});

app.listen(PORT);
console.log('Running on [http://localhost:'](http://localhost:%27) \+ PORT);
